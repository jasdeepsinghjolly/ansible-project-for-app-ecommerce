# Introduction

This is a sample e-commerce application built for learning purposes.

Architecture 1 ansible controller and 2 target nodes; make sure there is password ssh enabled between all instances and for the sake of easiness we are keeping all instances within the same AZ and same Security group. Also an user "ansible" is created (with sudo rights) in all nodes involved.



This is a sample e-commerce application built for learning purposes.

To configure EC2 ansible-controller:

- wget https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
- yum install -y epel-release-latest-7.noarch.rpm
- yum update -y
- yum install git python python2-pip openssl ansible python-level -y

- Now ,update /etc/ansible/hosts file and update the private IP's of target nodes.
  - And , uncomment below 2 lines from /etc/ansible/ansible.cfg 
        -  uncomment inventory=/etc/ansible/hosts file as an inventory file.
        - also uncomment sudo_user=root

- Now follow below steps on all nodes ( controller and target nodes):
    - adduser ansible
    - passwd ansible
    - To give sudo rights for ansible user 
        - do "visudo" as root and then modify as below:
                ## Allow root to run any commands anywhere
                root    ALL=(ALL)       ALL
                ansible    ALL=(ALL)       NOPASSWD: ALL
    - Update /etc/ssh/sshd_config
        # To disable tunneled clear text passwords, change to no here!
            PasswordAuthentication yes
            #PermitEmptyPasswords no
            #PasswordAuthentication no
            PermitRootLogin yes
    - now restart ssh service "service sshd restart"
    - To enable passwordless login follow below steps on all nodes:
        - ssh-keygen
        - ssh-copy-id ansible@<privateIP of other 2nodes>
    - Now , execute below playbooks "using ansible user" from ansible-controller one by one and provide ansible password when asked for sudo rights:
        logs:
        - [ansible@ip-172-31-47-75 ansible_project]$ ll
        - total 12
        - -rw-rw-r-- 1 ansible ansible  483 Jul 14 05:32 db-load-script.sql
        - -rw-rw-r-- 1 ansible ansible 1882 Jul 14 09:13 playbook_to_configure_http.yml
        - -rw-rw-r-- 1 ansible ansible 2425 Jul 14 09:13 playbook_to_install_mysql_db.yml
        - [ansible@ip-172-31-47-75 ansible_project]$ 
        - [ansible@ip-172-31-47-75 ansible_project]$ 
        - [ansible@ip-172-31-47-75 ansible_project]$ ansible-playbook playbook_to_install_mysql_db.yml -K
        - [ansible@ip-172-31-47-75 ansible_project]$ ansible-playbook playbook_to_configure_http.yml -K
        - [ansible@ip-172-31-47-75 ansible_project]$


Here's the manual deployment AWS instances which we have automated using Ansible-playbooks.

## Deploy Pre-Requisites

1. Install FirewallD

```
sudo yum install -y firewalld
sudo service firewalld start
sudo systemctl enable firewalld
```

## Deploy and Configure Database

1. Install mysql

```
sudo yum install -y mysql-server
sudo vi /etc/my.cnf
sudo service mysql start
sudo systemctl enable mysql
```

2. Configure firewall for Database

```
sudo firewall-cmd --permanent --zone=public --add-port=3306/tcp
sudo firewall-cmd --reload
```

3. Configure Database

```
$ mysql
MariaDB > CREATE DATABASE ecomdb;
MariaDB > CREATE USER 'ecomuser'@'localhost' IDENTIFIED BY 'ecompassword';
MariaDB > GRANT ALL PRIVILEGES ON *.* TO 'ecomuser'@'localhost';
MariaDB > FLUSH PRIVILEGES;
```

> ON a multi-node setup remember to provide the IP address of the web server here: `'ecomuser'@'web-server-ip'`

4. Load Product Inventory Information to database

Create the db-load-script.sql

```
cat > db-load-script.sql <<-EOF
USE ecomdb;
CREATE TABLE products (id mediumint(8) unsigned NOT NULL auto_increment,Name varchar(255) default NULL,Price varchar(255) default NULL, ImageUrl varchar(255) default NULL,PRIMARY KEY (id)) AUTO_INCREMENT=1;

INSERT INTO products (Name,Price,ImageUrl) VALUES ("Laptop","100","c-1.png"),("Drone","200","c-2.png"),("VR","300","c-3.png"),("Tablet","50","c-5.png"),("Watch","90","c-6.png"),("Phone Covers","20","c-7.png"),("Phone","80","c-8.png"),("Laptop","150","c-4.png");

EOF
```

Run sql script

```

mysql < db-load-script.sql
```


## Deploy and Configure Web

1. Install required packages

```
sudo yum install -y httpd php php-mysql
sudo firewall-cmd --permanent --zone=public --add-port=80/tcp
sudo firewall-cmd --reload
```

2. Configure httpd

Change `DirectoryIndex index.html` to `DirectoryIndex index.php` to make the php page the default page

```
sudo sed -i 's/index.html/index.php/g' /etc/httpd/conf/httpd.conf
```

3. Start httpd

```
sudo service httpd start
sudo systemctl enable httpd
```

4. Download code

```
sudo yum install -y git
git clone https://github.com/kodekloudhub/learning-app-ecommerce.git /var/www/html/
```

5. Update index.php

Update [index.php](https://github.com/kodekloudhub/learning-app-ecommerce/blob/13b6e9ddc867eff30368c7e4f013164a85e2dccb/index.php#L107) file to connect to the right database server. In this case `localhost` since the database is on the same server.

```
sudo sed -i 's/172.20.1.101/localhost/g' /var/www/html/index.php

              <?php
                        $link = mysqli_connect('172.20.1.101', 'ecomuser', 'ecompassword', 'ecomdb');
                        if ($link) {
                        $res = mysqli_query($link, "select * from products;");
                        while ($row = mysqli_fetch_assoc($res)) { ?>
```

> ON a multi-node setup remember to provide the IP address of the database server here.
```
sudo sed -i 's/172.20.1.101/localhost/g' /var/www/html/index.php
```

6. Test

```
curl http://localhost
```
